import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DefaultComponent} from './default.component';
import {SharedModule} from '../shared/shared.module';
import {AppRoutingModule} from '../app-routing.module';
import {MatSidenavModule} from '@angular/material';
import {DashboardModule} from '../dashboard/dashboard.module';
import {DashboardTestComponent} from '../dashboard/dashboard-test/dashboard-test.component';
import {PagesComponent} from '../dashboard/pages/pages.component';


@NgModule({
  declarations: [DefaultComponent],
  imports: [
    CommonModule,
    SharedModule,
    AppRoutingModule,
    MatSidenavModule,
    DashboardModule
  ],
  entryComponents: [
    DashboardTestComponent,
    PagesComponent
  ]
})
export class DefaultModule {
}
