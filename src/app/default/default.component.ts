import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-default',
  templateUrl: './default.component.html',
  styleUrls: ['./default.component.scss']
})
export class DefaultComponent implements OnInit {

  sidenavToggle = true;

  constructor() {
  }

  ngOnInit() {
  }

  sidenavToggler($event) {
    this.sidenavToggle = !this.sidenavToggle;
  }
}
