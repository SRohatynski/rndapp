import { Component, OnInit } from "@angular/core";
import { GridsterConfig } from "angular-gridster2";
import { DashboardService } from "../services/dashboard/dashboard.service";
import { EnvironmentalDataService } from "../services/environmentalData/environmentalData.service";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.scss"]
})
export class DashboardComponent implements OnInit {
  dynamicComponent;

  selected = null;

  constructor(
    private dashboardService: DashboardService,
    private environmentalDataService: EnvironmentalDataService
  ) {}

  ngOnInit() {
    this.environmentalDataService.getEnvironmentalData();
  }

  dashboard() {
    this.dashboardService.getDashboard();
  }

  get options(): GridsterConfig {
    return this.dashboardService.options;
  }

  removeItem($event, item) {
    this.dashboardService.deleteItem($event, item.id);
  }

  addItem(component?: Component) {
    console.log(component);
    if (component) {
      this.dynamicComponent = component;
      this.dashboardService.addItem();
    } else {
      this.dashboardService.addItem();
    }
  }
}
