import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { DashboardComponent } from "./dashboard.component";
import {
  MatChipsModule,
  MatDividerModule,
  MatFormFieldModule,
  MatIconModule,
  MatOptionModule,
  MatSelectModule
} from "@angular/material";
import { GridsterModule } from "angular-gridster2";
import { PagesComponent } from "./pages/pages.component";
import { FormsModule } from "@angular/forms";
import { DashboardTestComponent } from "./dashboard-test/dashboard-test.component";
import { EnvironmentalDataService } from "../services/environmentalData/environmentalData.service";
import {AgChartsAngularModule} from 'ag-charts-angular';
import {NgxEchartsModule} from 'ngx-echarts';
import * as echarts from 'echarts';

@NgModule({
  declarations: [DashboardComponent, PagesComponent, DashboardTestComponent],
  imports: [
    CommonModule,
    MatIconModule,
    MatChipsModule,
    MatFormFieldModule,
    MatOptionModule,
    MatDividerModule,
    GridsterModule,
    MatSelectModule,
    FormsModule,
    AgChartsAngularModule,
    NgxEchartsModule.forRoot({
      echarts
    })
  ],
  providers: [EnvironmentalDataService]
})
export class DashboardModule {}
