import { Component, OnInit } from '@angular/core';
import {EnvironmentalDataService} from '../../services/environmentalData/environmentalData.service';

@Component({
  selector: 'app-dashboard-test',
  templateUrl: './dashboard-test.component.html',
  styleUrls: ['./dashboard-test.component.scss']
})
export class DashboardTestComponent implements OnInit {

  options: any;
  data: any;
  timer: any;
  updateOptions: any;

  xAxisData = [];
  data1 = [];
  data2 = [];



  constructor( private environmentalDataService: EnvironmentalDataService) { }

  ngOnInit() {

    for (let i = 0; i < 100; i++) {
      this.xAxisData.push();
      this.data1.push((Math.sin(i / 5) * (i / 5 - 10) + i / 6) * 5);
      this.data2.push((Math.cos(i / 5) * (i / 5 - 10) + i / 6) * 5);
    }

    this.options = {
      legend: {
        data: ['Temperature', 'Distance'],
        align: 'left',
      },
      tooltip: {},
      xAxis: {
        data: this.xAxisData,
        silent: false,
        splitLine: {
          show: false,
        },
      },
      yAxis: {},
      series: [
        {
          name: 'Temperature',
          type: 'bar',
          data: this.data1,
          animationDelay: (idx) => idx * 10,
        },
        {
          name: 'Distance',
          type: 'bar',
          data: this.data2,
          animationDelay: (idx) => idx * 10 + 100,
        },
      ],
      animationEasing: 'elasticOut',
      animationDelayUpdate: (idx) => idx * 5,
    };

    // Mock dynamic data:
    this.timer = setInterval(() => {
      this.fetchData();
      // update series data:
      this.updateOptions = {
        xAxis: {
          data: this.xAxisData,
          silent: false,
          splitLine: {
            show: false,
          },
        },
        yAxis: {},
        series: [
          {
            name: 'Temperature',
            type: 'bar',
            data: this.data1,
            animationDelay: (idx) => idx * 10,
          },
          {
            name: 'Distance',
            type: 'bar',
            data: this.data2,
            animationDelay: (idx) => idx * 10 + 100,
          }]
      };
    }, 3000);
  }

  fetchData() {
    this.environmentalDataService.getEnvironmentalData().subscribe(data => {
      let xAxis = [];
      let dat1 = [];
      let dat2 = [];
      for (let dat of data) {
        let res = dat.message.replace('{', '')
          .replace('}', '')
          .replace(/\s+/g,'')
          .split(",");
        dat.temperature = +res[0].replace('"temperature":','');
        dat.distance = +res[1].replace('"distance":','');
        dat.motion = +res[2].replace('"motion":','');
        xAxis.push(dat.id);
        dat1.push(dat.distance);
        dat2.push(dat.temperature);
        console.log(data);
      }
      this.xAxisData = xAxis;
      this.data1 = dat1;
      this.data2 = dat2;
    });
  }

}
