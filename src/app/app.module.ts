import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { DefaultModule } from "./default/default.module";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { EnvironmentalDataService } from "./services/environmentalData/environmentalData.service";
import { HttpClientModule } from "@angular/common/http";
import {AgChartsAngularModule} from 'ag-charts-angular';
import {PagesComponent} from './dashboard/pages/pages.component';
import {NgxEchartsModule} from 'ngx-echarts';
import * as echarts from 'echarts';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DefaultModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AgChartsAngularModule,
    NgxEchartsModule.forRoot({
      echarts
    })
  ],
  bootstrap: [AppComponent],
  entryComponents: [PagesComponent]
})
export class AppModule {}
