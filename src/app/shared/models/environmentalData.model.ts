export interface EnvironmentalData {
  id: string;
  message: string;
  temperature: number;
  motion: number;
  distance: number;
}
