import {Injectable} from '@angular/core';
import {CompactType, DisplayGrid, GridsterConfig, GridsterItem, GridType} from 'angular-gridster2';
import {PagesComponent} from '../../dashboard/pages/pages.component';
import {DashboardTestComponent} from '../../dashboard/dashboard-test/dashboard-test.component';

const availableComponents = [
  {name: 'Environmental Data', id: '1', component: DashboardTestComponent}
];

@Injectable({
  providedIn: 'root'
})

export class DashboardService {
  public dashboard: GridsterItem[] = [];

  id: string;

  public options: GridsterConfig = {
    draggable: {
      enabled: true
    },
    pushItems: true,
    resizable: {
      enabled: true
    }
  };

  constructor() {
    this.options = {
      gridType: GridType.Fit,
      compactType: CompactType.None,
      margin: 10,
      outerMargin: true,
      outerMarginTop: null,
      outerMarginRight: null,
      outerMarginBottom: null,
      outerMarginLeft: null,
      useTransformPositioning: true,
      mobileBreakpoint: 640,
      minCols: 1,
      maxCols: 100,
      minRows: 1,
      maxRows: 100,
      maxItemCols: 100,
      minItemCols: 1,
      maxItemRows: 100,
      minItemRows: 1,
      maxItemArea: 2500,
      minItemArea: 1,
      defaultItemCols: 1,
      defaultItemRows: 1,
      fixedColWidth: 100,
      fixedRowHeight: 100,
      keepFixedHeightInMobile: false,
      keepFixedWidthInMobile: false,
      scrollSensitivity: 10,
      scrollSpeed: 20,
      enableEmptyCellClick: false,
      enableEmptyCellContextMenu: false,
      enableEmptyCellDrop: false,
      enableEmptyCellDrag: false,
      enableOccupiedCellDrop: false,
      emptyCellDragMaxCols: 50,
      emptyCellDragMaxRows: 50,
      ignoreMarginInRow: false,
      draggable: {
        enabled: true,
      },
      resizable: {
        enabled: true,
      },
      swap: true,
      pushItems: true,
      disablePushOnDrag: false,
      disablePushOnResize: false,
      pushDirections: {north: true, east: true, south: true, west: true},
      pushResizeItems: true,
      displayGrid: DisplayGrid.None,
      disableWindowResize: false,
      disableWarnings: false,
      scrollToNewItems: false
    };
  }


  addItem(minItemRows?: number, maxItemCols?: number, label?: string) {
    this.dashboard.push(
      {
        x: 0,
        y: 0,
        cols: 10,
        rows: 10,
        id: Math.random()
      });
  }

  deleteItem($event, id: string): void {
    $event.preventDefault();
    $event.stopPropagation();
    const item = this.dashboard.find(d => d.id === id);
    this.dashboard.splice(this.dashboard.indexOf(item), 1);
  }

  getDashboard() {
    return this.dashboard;
  }

  setDropId(dropId: string): void {
    this.id = dropId;
  }

  changedOptions() {
    if (this.options.api && this.options.api.optionsChanged) {
      this.options.api.optionsChanged();
    }
  }

  getAvailableComponents() {
    return availableComponents;
  }

}
