import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { EnvironmentalData } from "src/app/shared/models/environmentalData.model";

@Injectable()
export class EnvironmentalDataService {
  constructor(private http: HttpClient) {}

  public getEnvironmentalData() {
    return this.http
      .get<EnvironmentalData[]>(
        "https://environmentaldataservice.azurewebsites.net/environmentaldata/getNewest"
      );
  }
}
